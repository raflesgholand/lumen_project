<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;


class UserController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware("login");
    }

    public function index()
    {

       
        $data = [
            "message" => "Succes Login. Ypu have rights to access data"
        ];
        return response()->json($data, 200);
    
    }

    public function getIndexViews()
    {
        $title='Belajar Lumen LOREM IPSUM';
        $keluarga = array('Rafles','Budi','Mawar','andi','mira');
        return view('user',compact('title','keluarga'));
    }

}